var readline = require("readline");

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let nilaiSiswa = [];
console.log(
  "------------------------------------------------------\nMENGHITUNG NILAI SISWA (input 'q' jika sudah selesai)\n------------------------------------------------------"
);
var pertanyaan = function () {
  rl.question("Masukkan Nilai Siswa: ", function (inputNilai) {
    nilaiSiswa.push(inputNilai);
    if (inputNilai == "q") {
      nilaiSiswa.pop();

      function ratarataNilai(nilaiSiswa) {
        var sum = 0;
        for (var i = 0; i < nilaiSiswa.length; i++) {
          sum += parseFloat(nilaiSiswa[i]);
          var avg = sum / nilaiSiswa.length;
        }
        return avg;
      }

      function sortNilai(nilaiSiswa) {
        var len = nilaiSiswa.length;
        for (var i = len - 1; i >= 0; i--) {
          for (var j = 1; j <= i; j++) {
            if (nilaiSiswa[j - 1] > nilaiSiswa[j]) {
              var temp = nilaiSiswa[j - 1];
              nilaiSiswa[j - 1] = nilaiSiswa[j];
              nilaiSiswa[j] = temp;
            }
          }
        }
        return nilaiSiswa;
      }

      function filterKKM(nilaiSiswa) {
        let lolos = [];
        let tidakLolos = [];

        nilaiSiswa.forEach((nilaiSiswa) => {
          if (nilaiSiswa < 75) {
            tidakLolos.push(nilaiSiswa);
          } else {
            lolos.push(nilaiSiswa);
          }
        });
        return (
          "Siswa yang lolos ada " +
          lolos.length +
          ", dan yang tidak lolos ada " +
          tidakLolos.length
        );
      }

      let tampilNilai = nilaiSiswa.toString();

      console.log("\nDaftar Nilai Siswa: " + tampilNilai);
      console.log("\nNilai terendah = " + Math.min.apply(Math, nilaiSiswa));
      console.log("Nilai tertinggi = " + Math.max.apply(Math, nilaiSiswa));
      console.log("Rata-rata nilai = " + ratarataNilai(nilaiSiswa));
      console.log("Urutan nilai dari terendah ke tertinggi = " + sortNilai(nilaiSiswa));
      console.log(filterKKM(nilaiSiswa));

      return rl.close();
    }
    pertanyaan();
  });
};

pertanyaan();
