const readline = require('readline');

var connectrl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var pilihan;
var sisi;
var r;
var t;
var a;
var b;

function tambah(a, b){
    return a + b;
}

function kurang(a, b){
    return a - b;
}
function kali(a, b){
    return a * b;
}

function bagi(a, b){
    return a / b;
}

function akar(a){
    return Math.sqrt(a);
}

function luasPersegi(sisi){
    return sisi * sisi;
}

function volumeKubus(sisi){
    return sisi * sisi * sisi;
}

function volumeTabung(r, t){
    return Math.PI * r * r * t;
}

console.log("--Kalkulator Aritmatika--\n========================\nPilih operasi hitung \n\t1. Tambah (+)\n\t2. Kurang (-)\n\t3. Kali (*)\n\t4. Bagi (/)\n\t5. Akar Kuadrat\n\t6. Luas Persegi\n\t7. Volume Kubus\n\t8. Volume Balok")

connectrl.question("Pilihan anda : ", (pilihan)=>{
    if (pilihan == 1){
        connectrl.question("Masukkan nilai pertama : ", (a)=>{
            connectrl.question("Masukkan nilai kedua : ", (b)=>{
                console.log("Hasil pertambahannya adalah : " + tambah(+a, +b))
            })
        })
    }
    else if (pilihan == 2){
        connectrl.question("Masukkan nilai pertama : ", (a)=>{
            connectrl.question("Masukkan nilai kedua : ", (b)=>{
                console.log("Hasil pengurangannya adalah : " + kurang(+a, +b))
            })
        })
    }
    else if (pilihan == 3){
        connectrl.question("Masukkan nilai pertama : ", (a)=>{
            connectrl.question("Masukkan nilai kedua : ", (b)=>{
                console.log("Hasil perkaliannya adalah : " + kali(+a, +b))
            })
        })
    }
    else if (pilihan == 4){
        connectrl.question("Masukkan nilai pertama : ", (a)=>{
            connectrl.question("Masukkan nilai kedua : ", (b)=>{
                console.log("Hasil pembagiannya adalah : " + bagi(+a, +b))
            })
        })
    }
    
    else if (pilihan == 5){
        connectrl.question("Masukkan nilai : ", (a)=>{
            console.log("Akar kuadratnya adalah : "+ akar(+a))
        })
    }
    else if (pilihan == 6){
        connectrl.question("Masukkan nilai sisi : ", (sisi)=>{
            console.log("Luas persegi adalah : "+ luasPersegi(+sisi))
        })
    }
    else if (pilihan == 7){
        connectrl.question("Masukkan nilai sisi : ", (sisi)=>{
            console.log("Volume kubus adalah : "+ volumeKubus(+sisi))
        })
    }
    else if (pilihan == 8){
        connectrl.question("Masukkan nilai r : ", (r)=>{
            connectrl.question("Masukkan nilai t : ", (t)=>{
                console.log("Hasil volume tabung adalah : " + volumeTabung(+r, +t))
            })
        })
    }
    else{
        console.log("Kesalahan input.")
        connectrl.close()
    }

})