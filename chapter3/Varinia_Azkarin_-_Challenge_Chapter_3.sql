-- POSTGRESQL CRUD IMPLEMENTATION

CREATE DATABASE epixgames;
CREATE TABLE user_game(
    user_id BIGSERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE user_game_biodata(
    biodata_code BIGSERIAL PRIMARY KEY,
    user_id BIGINT,
    display_name VARCHAR(255),
    description TEXT,
    gender VARCHAR(255),
    age INT
);

CREATE TABLE user_game_history(
    history_id BIGSERIAL PRIMARY KEY,
    game_id BIGINT,
    user_id BIGINT,
    exp_user BIGINT
);

CREATE TABLE games(
    game_id BIGSERIAL PRIMARY KEY,
    game_name VARCHAR(255) NOT NULL,
    genre VARCHAR(255) NOT NULL
);

--INSERT DATA user_game
INSERT INTO user_game(username, password) VALUES(
    'alpha', 'abc'
);
INSERT INTO user_game(username, password) VALUES(
    'bravo', 'abc'
);
INSERT INTO user_game(username, password) VALUES(
    'charlie', 'abc'
);
INSERT INTO user_game(username, password) VALUES(
    'delta', 'abc'
);
INSERT INTO user_game(username, password) VALUES(
    'echo', 'abc'
);
INSERT INTO user_game(username, password) VALUES(
    'foxtrot', 'abc'
);
INSERT INTO user_game(username, password) VALUES(
    'gajadi', 'abc'
);

-- INSERT DATA user_game_biodata
INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '1', 'al pha', 'asdfghjkl', 'male', '12'
);

INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '2', 'br avo', 'asdfghjkl', 'male', '13'
);

INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '3', 'charlie puth', 'asdfghjkl', 'male', '20'
);

INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '4', 'del ta', 'asdfghjkl', 'female', '22'
);

INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '5', 'echo patrio', 'asdfghjkl', 'male', '23'
);

INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '6', 'fox trot', 'asdfghjkl', 'male', '32'
);

INSERT INTO user_game_biodata(user_id, display_name, description, gender, age) VALUES(
    '7', 'gak jadi', 'asdfghjkl', 'male', '26'
);

--INSERT TABLE GAMES
INSERT INTO games(game_name, genre) VALUES(
    'Counter Strike', 'FPS'
);
INSERT INTO games(game_name, genre) VALUES(
    'Minecraft', 'Open World'
);
INSERT INTO games(game_name, genre) VALUES(
    'Genshin Impact', 'RPG'
);
INSERT INTO games(game_name, genre) VALUES(
    'GTA V', 'Open World'
);
INSERT INTO games(game_name, genre) VALUES(
    'Doki Doki Literature Club', 'Visual Novel'
);
INSERT INTO games(game_name, genre) VALUES(
    'Mobile Legends', 'MOBA'
);
INSERT INTO games(game_name, genre) VALUES(
    'Valorant', 'FPS'
);


--INSERT DATA USER GAME HISTORY
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '1', '1', '2500'
);
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '2', '2', '2300'
);
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '3', '3', '2300'
);
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '4', '4', '2300'
);
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '5', '5', '2300'
);
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '6', '6', '2300'
);
INSERT INTO user_game_history(game_id, user_id, exp_user) VALUES(
    '7', '7', '2300'
);

--Mengganti Tipe Data Column
ALTER TABLE user_game_biodata
ALTER COLUMN user_id 
TYPE BIGINT
USING user_id::bigint;

--Menambahkan Column Baru
ALTER TABLE user_game_history
ADD time_played TIME;

--SELECT TABLE--
--Menampilkan Data Tabel user_game
SELECT *
FROM user_game;

--Menampilkan Data Tabel user_game_biodata
SELECT *
FROM user_game_biodata;

--Menampilkan Data Tabel games
SELECT *
FROM games;

--Menampilkan Data Tabel user_game_history
SELECT *
FROM user_game_history;

--Menampilkan Tabel Biodata dengan username
SELECT *, user_game.username
FROM user_game_biodata
JOIN user_game ON user_game.user_id = user_game_biodata.user_id;



--UPDATE TABLE
UPDATE user_game_biodata 
SET display_name = 'Jadi'
WHERE user_id = 7; 

UPDATE user_game
SET username ='jadi'
WHERE user_id = 7;

--DELETE
DELETE FROM user_game
WHERE user_id = 7;


